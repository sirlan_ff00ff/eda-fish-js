function save_notepad() {
	const a = document.getElementById('notetext')
	if (a) {
		const n = a.value
		sessionStorage.npad = n
	}
}

function load_notepad() {
	const a = document.getElementById('notetext')
	const b = sessionStorage.npad
	if ((a)&&(b)) {
		a.value = b
	}
}

load_notepad()
