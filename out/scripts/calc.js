let nu_btn
let op_btn
let eq_btn
let dl_btn
let cl_btn 
let ou_dsp

function init_calc() {
	nu_btn = document.querySelectorAll('[data-number]')   
	op_btn = document.querySelectorAll('[data-operation]')
	eq_btn = document.querySelector('[data-equals]')
	dl_btn = document.querySelector('[data-delete]')
	cl_btn = document.querySelector('[data-clear]')
	ou_dsp = document.querySelector('[data-out]')

	if (eq_btn == null) {console.log('Calculator body not found.'); return}

	nu_btn.forEach(but => {
		but.addEventListener('click', () => {
			if (error) {return}
			caddNum(but.innerText)
			cupDis()
		})
	})

	op_btn.forEach(but => {
		but.addEventListener('click', () => {
			if (error) {return}
			cchOp( but.innerText )
		})
	})

	eq_btn.addEventListener('click', button => {
		if (error) {return}
		calculate()
		cupDis()
	})

	cl_btn.addEventListener('click', button => {
		cclear()
		cupDis()
	})
	dl_btn.addEventListener('click', button => {
		if (error) {return}
		cdel()
		cupDis()
	})
	
	cclear()
}

let c_number = ''
let p_number = 0
let has_pt = false
let computed = false
let ope = null;
let error = false;

function cclear() {
	c_number = '';
	p_number = 0;
	has_pt = false;
	error = false;
	ope = null;
	cupDis();
}
function cdel() {
	c_number = c_number.toString().slice(0,-1)
}
function caddNum(nu) {
	if (c_number.length >= 17){return}
	if ((nu == '.')&&(c_number.includes('.'))){return}
	c_number = c_number.toString() + nu.toString()
}
function cchOp(op) {
	if (c_number == ''){ return }
	if (p_number !== ''){ calculate() }
	ope = op.toString()
	p_number = c_number
	c_number = ''
}
function calculate() {
	let res = 0;
	const pn = parseFloat(p_number)
	const cn = parseFloat(c_number)
	if ( isNaN(pn) || isNaN(cn)){return}
	if ( (cn == 0)&&(ope=='/') ){cerror('Division by zero.');return}
	switch (ope) {
		case '+': res = pn + cn; break
		case '-': res = pn - cn; break
		case '*': res = pn * cn; break
		case '/': res = pn / cn; break
		default : return
	}
	if ( res.toString().length > 17 ){cerror('Number too big.');return}
	c_number = res
	ope = null
	p_number = ''
	has_pt = ''
}
function cupDis() {
	ou_dsp.innerText = c_number
}
function cerror(e_type){
	c_number = e_type
	error = true
}

