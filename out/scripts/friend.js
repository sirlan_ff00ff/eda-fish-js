var petQ = [
	"Thankies",
	"Yay, pets!",
	"♥♥♥",
	"<em>purr purr</em>",
	":3", 
	"uwu"	
];
var is_pet = false
const tm = 3
function pet(s) {
	if (is_pet) {return}
	is_pet = true

	const q = petQ[ Math.floor( Math.random() * petQ.length ) ];
	display(q,tm*1000)
	
	setTimeout(function(){
		is_pet = false;
	},tm*1000)
}

var askQ = [
	"What about some pets?",
	"Give me pets?",
	"I want pets",
	"Pets, please :3",
	"uwu",
	"How's you, friend?",
	":3",
	"<em>rawr</em>",
	"<em>raaaaawwwwrrr</em>",
	"Look, I'm a dragon",
	"Hi, friend",
	"<em>zzzz</em>",
	"Double click to pet me!"
];
function ask() {
	if (!is_pet) {
		const q = askQ[ Math.floor( Math.random() * askQ.length ) ];
		display(q,tm*(1.2)*1000)
	}

	setTimeout(function(){
		ask()
	},between(40,90)*1000)
}
setTimeout(function(){
	ask()
},between(10,20)*1000)

function display(mess,time) {
	const text = $('#friend .la')[0]
	text.innerHTML = mess
	text.style.opacity = '75%';
	setTimeout(function(){
		text.style.opacity = '0%';
	},time)

}

function between(min, max) {  
  return Math.floor(
    Math.random() * (max - min) + min
  )
}

//addDTap($('#friend #pett')[0], function(){ pet() })

