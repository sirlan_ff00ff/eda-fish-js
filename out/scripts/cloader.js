function include() {
	const INCLUDE_ATTR = "data-inc";
	const FAILURE = "<span class='load-failure'>Part of this site could not load</span>";

	document.addEventListener("DOMContentLoaded", function(event) {
		for(const include of document.querySelectorAll(`[${INCLUDE_ATTR}]`)) {
			const xhttp = new XMLHttpRequest();
			
			xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					include.removeAttribute(INCLUDE_ATTR);
					include.setAttribute(`${INCLUDE_ATTR}-loaded`,"");
					include.innerHTML = xhttp.responseText;
				}
				else {
					include.innerHTML = FAILURE;
				}
			}
			
			xhttp.open("GET", include.getAttribute(INCLUDE_ATTR), true);
			xhttp.send();
		}  
	});
}
