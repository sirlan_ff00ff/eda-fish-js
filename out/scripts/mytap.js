const ttimeout = 300 // miliseconds
function addTap(o, fn) {
	let touched = false;
	o.addEventListener('touchstart', function(e){ 
		touched = true

		setTimeout(function(){
			touched = false
		},ttimeout)
	})
	o.addEventListener('touchend', function(e){ 
		if (touched) {
			fn
		}
	})
}
function addDTap(o, fn) {
	let touched = false;
	let dt = 0
	o.addEventListener('touchstart', function(e){ 
		touched = true

		setTimeout(function(){
			touched = false
		},ttimeout)
		setTimeout(function(){
			if (dt >= 1) {
				dt = 0
			}
		},ttimeout*1.7)
	})
	o.addEventListener('touchend', function(e){ 
		if (touched) {
			dt += 1
		}
		if (dt == 2) {
			fn()
		}
	})
}

const sto = document.querySelectorAll('[data-tap]')
const dto = document.querySelectorAll('[data-dbtap]')

sto.forEach(ob => {
	addTap(ob, function(){ eval(ob.dataset.tap) })
})
dto.forEach(ob => {
	addDTap(ob, function(){ eval(ob.dataset.dbtap) })
})
