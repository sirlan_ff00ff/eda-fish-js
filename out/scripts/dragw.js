let h_pad = 27
let w_pad = 20
let bar_height = 1;
const min_w = 385
const min_h = 150
var window_ = [];
var icons_ = [];
let vw = window.innerWidth
let vh = window.innerHeight
var hid_wins = [];
var hid_owsz = [];
var is_touch = false;
var first_roll = true;

// do stuff
function getTranslateValues(element) {
  const style = window.getComputedStyle(element)
  const matrix = style['transform'] || style.webkitTransform || style.mozTransform
  // No transform property. Simply return 0 values.
  if (matrix === 'none' || typeof matrix === 'undefined') {
	return {
	  x: 0,
	  y: 0
	}
  }
  const matrixType = matrix.includes('3d') ? '3d' : '2d'
  const matrixValues = matrix.match(/matrix.*\((.+)\)/)[1].split(', ')
  if (matrixType === '2d') {
	return {
	  x: matrixValues[4],
	  y: matrixValues[5]
	}
  }
  if (matrixType === '3d') {
	return {
	  x: matrixValues[12],
	  y: matrixValues[13]
	}
  }
}

function translateXY(v, y) {
  return 'translate(' + v + ',' + y + ')';
}
function removeItem(arr, value) {
  var index = arr.indexOf(value);
  if (index > -1) {
	arr.splice(index, 1);
  }
  return arr;
}

function dragElement(elmnt, group) {
  var pos1 = 0,
	pos2 = 0,
	pos3 = 0,
	pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
	// if present, the header is where you move the DIV from:
	document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
	document.getElementById(elmnt.id + "header").ontouchstart = dragMouseDown;
  } else {
	// otherwise, move the DIV from anywhere inside the DIV:
	elmnt.onmousedown = dragMouseDown;
	elmnt.ontouchstart = dragMouseDown;
  }
  var mdown = false

  function dragMouseDown(e) {
	if (e.type == "touchstart") {
	  is_touch = true
	} else {
	  is_touch = false
	}
	e = e || window.event;
	e.preventDefault();
	// get the mouse cursor position at startup:
	pos3 = e.clientX;
	pos4 = e.clientY;
	if (!is_touch) {
	  document.onmouseup = closeDragElement;
	} else {
	  document.ontouchend = closeDragElement;
	}
	// call a function whenever the cursor moves:
	if (!is_touch) {
	  document.onmousemove = elementDrag;
	} else {
	  document.ontouchmove = elementDrag;
	}
	if (!mdown) {
	  group = removeItem(group, elmnt)
	  group.push(elmnt)
	  for (var i = 0; i < group.length; i++) {
		group[i].style.zIndex = i;
	  }
	  var mdown = true
	}
  }

  function elementDrag(e) {
	e = e || window.event;
	e.preventDefault();
	// calculate the new cursor position:
	if (!is_touch) {
	  pos1 = pos3 - e.clientX;
	  pos2 = pos4 - e.clientY;
	  pos3 = e.clientX;
	  pos4 = e.clientY;
	} else {
	  const ett = e.changedTouches[0]
	  pos1 = pos3 - ett.screenX;
	  pos2 = pos4 - ett.screenY;
	  pos3 = ett.screenX;
	  pos4 = ett.screenY;
	}
	// set the element's new position:
	const etv = getTranslateValues(elmnt)
	const ntop = (etv.y - pos2)
	const nlft = (etv.x - pos1)
	vw = window.innerWidth
	vh = window.innerHeight
	const ww = elmnt.clientWidth
	let nX = etv.x
	let nY = etv.y
	if ((ntop < vh - h_pad) && (ntop > bar_height)) {
	  nY = ntop
	}
	if ((nlft + (ww/4)< vw - w_pad) && (nlft + (ww * (2/4)) > 0)) {
	  nX = nlft
	}
	elmnt.style.transform = translateXY(nX + 'px', nY + 'px')
  }

  function closeDragElement() {
	// stop moving when mouse button is released:
	if (!is_touch) {
	  document.onmouseup = null;
	  document.onmousemove = null;
	} else {
	  document.ontouchend = null;
	  document.ontouchmove = null;
	}
	mdown = false
  }
}
//--- window resize
function makeResizableWindow(win) {

	const rh = win.querySelector(".brr");

	if (rh == undefined) {return}

	rh.onmousedown = res_ev
	rh.ontouchstart = res_ev

  function res_ev(e) {
	if (e.type == "touchstart") {
	  is_touch = true
	} else {
	  is_touch = false
	}
	//console.log(element[i].getBoundingClientRect())
	//console.log(e)
	res_w = e.target.parentElement
	e.preventDefault()
	if (!is_touch) {
	  original_mouse_y = e.pageY;
	} else {
	  original_mouse_y = e.changedTouches[0].screenY
	}
	window.addEventListener('mousemove', resize)
	window.addEventListener('touchmove', resize)
	window.addEventListener('mouseup', stopResize)
	window.addEventListener('touchend', stopResize)
  }

  function resize(e) {
	//console.log(e)
	const e_par = res_w
	let w_val
	let h_val
	if (!(e.type == "touchmove")) {
	  w_val = e.pageX - e_par.getBoundingClientRect().left
	  h_val = e.pageY - e_par.getBoundingClientRect().top
	} else {
	  const ett = e.changedTouches[0]
	  w_val = ett.screenX - e_par.getBoundingClientRect().left
	  h_val = ett.screenY - e_par.getBoundingClientRect().top
	}
	vw = window.innerWidth
	vh = window.innerHeight
	const ow = parseFloat(e_par.style.width)
	if ((w_val > min_w) && (w_val < vw * 0.99)) {
	  e_par.style.width = w_val + 'px'
	}
	if ((h_val > min_h) && !is_hidden(res_w) && (h_val < vh * 0.99 - bar_height)) {
	  e_par.style.height = h_val + 'px'
	}
	const etv= getTranslateValues(e_par)
	const etvx = parseFloat( etv.x )
	const ww = parseFloat( e_par.style.width )
	//console.log(etvx + ww/2 )
	//console.log('---')
	if ((etvx + (ww/2) <= 0) && (ww<ow)) {
		e_par.style.transform = translateXY( (-(ww/2)) + 'px', etv.y + 'px');
	}
  }

  function stopResize() {
	if (!is_touch) {
	  window.removeEventListener('mousemove', resize)
	} else {
	  window.removeEventListener('touchmove', resize)
	}
  }
}
/* makeResizableDiv(".resize")*/
// --- window hide
function is_hidden(w) {
  for (var i = 0; i < hid_wins.length; i++) {
	if (w == hid_wins[i]) {
	  return i + 1
	}
  }
  return false
}
var closeWin = []
//var closeWin = document.getElementsByClassName("close");
for (var i = 0; i < closeWin.length; i++) {
  make_clw(closeWin[i]);
}
function make_clw(win) {
  
	const a = document.getElementById(win.id + "header").getElementsByClassName("closebox")[0];

	if (a) {
	  a.onmousedown = clWin;
	  a.ontouchstart = clWin;
	} else {
		return
	}


  function clWin(e) {
	//win.style.visibility = "hidden"
	const is_h_p = is_hidden(win)
	if (is_h_p) {
	  opWin(is_h_p - 1);
	  return
	}
	hid_wins.push(win)
	hid_owsz.push(win.style.height)
	win.style.height = "27px";
	if (win.querySelectorAll(".wbottom").length > 0) {
	  win.querySelectorAll(".wbottom")[0].style.visibility = "hidden";
	  //document.querySelectorAll(win.id + " .wbottom")[0].style.visibility = "hidden";
	}
  }

  function opWin(wlpos) {
	win.style.height = hid_owsz[wlpos];
	document.getElementById(win.id + "header").style.background = "var(--ui_ac)";
	if (win.querySelectorAll(".wbottom").length > 0) {
	  win.querySelectorAll(".wbottom")[0].style.visibility = "visible";
	}
	//delete hid_wins[wlpos];
	hid_wins = removeItem(hid_wins, hid_wins[wlpos])
	//delete hid_owsz[wlpos];
	hid_owsz = removeItem(hid_owsz, hid_owsz[wlpos])
  }
}

function ecWin(win, cwl) {
  //win.style.visibility = "hidden"
  const is_h_p = is_hidden(win)
  if (is_h_p) {
	return
  }
  hid_wins.push(win)
	if (cwl != undefined) {
  		//console.log(cwl)
  		hid_owsz.push(cwl)
	} else {
  		hid_owsz.push(win.style.height)
	}
  win.style.height = "27px";
  if (win.querySelectorAll(".wbottom").length > 0) {
	win.querySelectorAll(".wbottom")[0].style.visibility = "hidden";
	//document.querySelectorAll(win.id + " .wbottom")[0].style.visibility = "hidden";
  }
}
function eoWin(win) {
	const wlpos = is_hidden(win)-1
	if (!(wlpos+1)) {
		return
	}
	win.style.height = hid_owsz[wlpos];
	document.getElementById(win.id + "header").style.background = "var(--ui_ac)";
	if (win.querySelectorAll(".wbottom").length > 0) {
	  win.querySelectorAll(".wbottom")[0].style.visibility = "visible";
	}
	//delete hid_wins[wlpos];
	hid_wins = removeItem(hid_wins, hid_wins[wlpos])
	//delete hid_owsz[wlpos];
	hid_owsz = removeItem(hid_owsz, hid_owsz[wlpos])
}
// -- make dialogs
var dials = []

function make_diali(win) {
  win.querySelectorAll(".wbottom .dcl")[0].addEventListener("click", clDial);

  function clDial(e) {
	win.style.visibility = "hidden";
	// change to actually delete window?
  }
}
// -- show window
function show_window(wid) {
  const w = document.getElementById(wid)
  w.style.visibility = "visible";
  windows_ = removeItem(windows_, w)
  windows_.push(w)
  for (var i = 0; i < windows_.length; i++) {
	windows_[i].style.zIndex = i;
  }
}

function togg_window(wid) {
  const w = document.getElementById(wid)
  if (!(w.style.visibility == "visible")) {
	w.style.visibility = "visible";
	windows_ = removeItem(windows_, w)
	windows_.push(w)
	for (var i = 0; i < windows_.length; i++) {
	  windows_[i].style.zIndex = i;
	}
  } else {
	w.style.visibility = "hidden";
  }
}

// a close/hide button
var hiddenWin = []

function make_hidew(win) {
	const a = document.getElementById(win.id + "header").getElementsByClassName("hidebox")[0];
	let has_action = false;

	if (a) {
		//console.log(a.dataset.act)
		if (a.dataset.act != undefined) {
			has_action = true
		}

		a.onmousedown = hideWin;
		a.ontouchstart = hideWin;
	}

	function hideWin(e) {
		//console.log(win.id)
		if (has_action) {
			eval(a.dataset.act + '()')
		}
		win.style.visibility = "hidden";
	}

}


// -- makewindow
function make_windows() {
  windows_ = Array.from(document.querySelectorAll(".window, .mmenu"));
  for (var i = 0; i < windows_.length; i++) {
	dragElement(windows_[i], windows_);
	makeResizableWindow(windows_[i])
	make_hidew(windows_[i]);
	make_clw(windows_[i]);
  }
  icons_ = Array.from(document.getElementsByClassName("dicon"));
  for (var i = 0; i < icons_.length; i++) {
	dragElement(icons_[i], icons_);
  }
  
  if (first_roll) {
	for (var i = 0; i < closeWin.length; i++) {
	  const wn = closeWin[i];
	  if (start_closed.includes(wn.id)) {
		//console.log(wn)
		ecWin(wn)
	  }
	}
	first_roll = false;
  }
  dials = document.getElementsByClassName("dial")
  for (var i = 0; i < dials.length; i++) {
	make_diali(dials[i]);
  }
	if (sessionStorage.getItem('w_data')) {
		load_windows()
	}
	if (sessionStorage.getItem('ic_data')) {
		load_icons()
	}
}
// Clock ...
function pad(n) {
	if (n.toString().length < 2) {
		return '0'+ n;
	} else { return n; }
}

function updateClock() {
  var now = new Date();
  var min = now.getMinutes(),
	hou = now.getHours()
  var tags = ["h", "m"],
	corr = [pad(hou), pad(min)];
  for (var i = 0; i < tags.length; i++) document.getElementById(tags[i]).firstChild.nodeValue = corr[i];
}

function initClock() {
  updateClock();
  window.setInterval("updateClock()", 15000);
}

// save/restore window positions
//

let save = true;

function clear_data() {
	sessionStorage.clear()
	//sessionStorage.removeItem("w_data")
	//sessionStorage.removeItem("ic_data")
	save = false;
	window.location.reload(false)
}

function st(o) {
	return JSON.stringify(o);
}
function us(j) {
	return JSON.parse(j);
}

function get_sWindows() {
	return us(sessionStorage.w_data)
}

function save_windows() {
	let wd = {}
	if (sessionStorage.getItem('w_data')) {
		wd = get_sWindows()
	} 
	for (var i = 0; i < windows_.length; i++) {
		const e = windows_[i]
		const etv = getTranslateValues(e)
		const ed = {
			'x' : etv.x,
			'y' : etv.y,
			'w' : parseFloat(e.clientWidth),
			'h' : parseFloat(e.clientHeight),
			'ih': Boolean(is_hidden(e)),
			'oh': '0',
			'zi': window.getComputedStyle(e).zIndex,
			'vi': window.getComputedStyle(e).visibility
		}
		const is_h_p = is_hidden(e)
		if (is_h_p) {
		  ed.oh =  hid_owsz[is_h_p - 1];
		}
		if (e.id != 'menu') {
			wd[e.id] = ed
		}
	} //---
	//console.log(wd)
	sessionStorage.w_data = st(wd)
}

function load_windows() {
	const wd = get_sWindows()
	for ( const [k,v] of Object.entries(wd) ) {
		//console.log( k )
		//console.log( v.w )
		let e = document.getElementById(k)
		if (e == null) { continue }

		e.style.transform = translateXY( v.x + 'px', v.y + 'px');
		e.style.width = v.w + 'px';	
		e.style.visibility = v.vi
		e.style.zIndex = v.zi

		if (v.ih) { ecWin(e, v.oh) } else { e.style.height = v.h + 'px' }
	}
}

function get_sIcons() {
	return us(sessionStorage.ic_data)
}

function save_icons() {
	let icd = {}
	if (sessionStorage.getItem('ic_data')) {
		icd = get_sIcons()
	} 
	for (var i = 0; i < icons_.length; i++) {
		const ic = icons_[i]
		const ictv = getTranslateValues(ic)
		const ed = {
			'x' : ictv.x,
			'y' : ictv.y,
			'zi': window.getComputedStyle(ic).zIndex
		}
		if (ic.id != "") {
			icd[ic.id] = ed
		}
	} //---
	sessionStorage.ic_data = st(icd)
}

function load_icons() {
	const icd = get_sIcons()
	//console.log(icd)
	for ( const [k,v] of Object.entries(icd) ) {
		let ic = document.getElementById(k)
		if (ic == null) { continue }

		ic.style.transform = translateXY( v.x + 'px', v.y + 'px');
		ic.style.zIndex = v.zi

	}
}

function save_layout() {
	save_windows();
	save_icons();
}

function cgoto(url) {
	window.location.href = url
}
