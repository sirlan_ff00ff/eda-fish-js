## 枝🐟 (Eda Fish) UI
A dumb attempt at a floating window layout.

An attempt at writting a site layout with floating windows that mimicks an OS 
(something more alike System 7 or IRIX).  
It's supposed to be a single main page (+login page) with each piece of content as a
window, more specific content can have It's own page if it's for the sake of simplicity
(like a game).

Written in HTML, CSS and JS + jQuery (the minified version included)

Stuff done:
* Windows, resize (bottom right), roll up/minimize, foreground on click, and close
* Desktop/folder icons
* List view (on posts)
* Window/icon postions and status saved on session storage, if you wanted you could save thse as cookies :P
* fake login
* posts loaded with AJAX
* calculator
* music player using the youtube embed api, it gives some resource error in the console, idk why
* desktop 'pet'

You can try it on [my site](https://sirlan-tomma.neocities.org/drag-ui/login.html).

### Testing:

On unix/-like systems: 

* Python 3
* Flask

`chmod +x webp.sh`  
`./webp.sh`  
Open `127.0.0.1:5000` on your browser  

### Licensing, because yes

This piece of software is distributed under the Mozilla Public License Version 2.0 (MPL 2.0)

jQuery is distributed under MIT license

