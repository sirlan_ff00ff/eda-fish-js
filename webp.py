#!/bin/python
# Only for testing purposes

from flask import Flask, send_from_directory
import os

app = Flask(__name__, static_url_path='', static_folder='out/')

@app.route('/')
def index():
    return app.send_static_file('login.html')

@app.errorhandler(404)
def page_not_found(e):
    return '<h2>Page not found, you silly</h2> go back to the <a href="login.html">start</a>.', 404
